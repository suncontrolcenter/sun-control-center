Sun Control Center is a residential and commercial window tinting company. It is the premier one-stop-shop that takes care of every aspect of the process; from design to installation. It was founded by Bob and Tate Meese in Huntington, IN in 1976. It remains a family-based business; providing tinting solutions to the people of Northern Indiana including those living in Fort Wayne, Elkhart, Mishawaka and surrounding areas.

Website : http://suncontrolcenter.com/
